FROM maven AS build
WORKDIR /opt/app
COPY ./ /opt/app
RUN mvn clean install -DskipTests

FROM openjdk:11
EXPOSE 8080
MAINTAINER idris.com
COPY --from=build /opt/app/target/*.jar app.jar
ENTRYPOINT ["java","-jar","app.jar"]
